using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


/// <summary> Reset XR-Rig to location </summary>

public class XRReset : MonoBehaviour
{
    //---------------------------------------------------------------------------------------------------
    [Header("XR-Rig Members")]
    [SerializeField] public GameObject resetPosition;
    [SerializeField] GameObject XRRig;
    [SerializeField] Camera MainCamera;

    /// <summary> Firing. </summary>
    public UnityEvent ResetXRRig = new UnityEvent();
    public KeyCode StartXRRigResetKey = KeyCode.Space;

    [Header("Move Reset Position")]
    [SerializeField] public float movementSpeed;
    public KeyCode isMovingUpKey = KeyCode.UpArrow;
    public KeyCode isMovingDownKey = KeyCode.DownArrow;


    [ContextMenu("Reset Position")]
    public void ResetPosition()
    {
        var rotationAngleY = MainCamera.transform.rotation.eulerAngles.y - resetPosition.transform.rotation.eulerAngles.y;
        XRRig.transform.Rotate(0, -rotationAngleY, 0);

        var distanceDiff = resetPosition.transform.position - MainCamera.transform.position;
        XRRig.transform.position += distanceDiff;

    }

    private void Update()
    {
        if(Input.GetKeyDown(StartXRRigResetKey))
        {
            this.ResetXRRig.Invoke();
        }

        if (Input.GetKeyDown(isMovingUpKey))
        {
            resetPosition.transform.position += new Vector3(0, +movementSpeed, 0 );
        }

        if (Input.GetKeyDown(isMovingDownKey))
        {
            resetPosition.transform.position += new Vector3(0, -movementSpeed, 0);
        }

    }



}

