using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class MeasureDrift : MonoBehaviour
{
    /// <summary> The hand linked to the arm. </summary>
    public SG.SG_TrackedHand SGTrackingSource;

    /// <summary> Find the distance between actual hand position and fake hand position. </summary>
    public GameObject FakeSGHand;
    public GameObject ActualSGHand;

    public float offsetInCM = 0f;

    //public DriftUpdater offsetSource;

    /// <summary> Assets that should be visible during calibration. </summary>
    public GameObject[] CalibrationAssets = new GameObject[0];

    /// <summary> Whether or not a Proprioception Test sequence is running at the moment. </summary>
    public bool startTest = false;


    /// <summary> Print Test Result </summary>
    public bool printResult = false;

    // Waveform and UI Text
    public SG.SG_CustomWaveform wf_calibrationStart;

    private bool playingWF = false;
    private Coroutine wfTimeOut = null;

    public UnityEngine.UI.Text TitleText;
    public UnityEngine.UI.Text InstructionText;


    // Distance Variables
    private Vector3 distance_Actual_Fake;
    private Vector3 distance_Virtual_Fake;

    public DriftUpdater DriftUpdaterSource;

    public KeyCode debugKey = KeyCode.D;


    private IEnumerator LockWavefromPlays(SG.SG_CustomWaveform toPlay)
    {
        playingWF = true;
        yield return new WaitForSeconds(toPlay.Duration);
        playingWF = false;
    }

    public void PlayWaveform(SG.SG_CustomWaveform toPlay, bool overridePlaying)
    {
        if (toPlay == null || this.SGTrackingSource == null)
        {
            return;
        }

        if (overridePlaying && this.wfTimeOut != null)
        {
            StopCoroutine(wfTimeOut);
            playingWF = false;
        }
        if (!playingWF) //Coroutine has stopped or been cancelled.
        {
            SGTrackingSource.SendCustomWaveform(toPlay, SG.VibrationLocation.Palm_PinkySide);
            SGTrackingSource.SendCustomWaveform(toPlay, SG.VibrationLocation.Palm_IndexSide);
            SGTrackingSource.SendCustomWaveform(toPlay, SG.VibrationLocation.Thumb_Tip);
            SGTrackingSource.SendCustomWaveform(toPlay, SG.VibrationLocation.Index_Tip);
            this.wfTimeOut = StartCoroutine(LockWavefromPlays(toPlay));
        }
    }

    public void SetCalibrationAssets(bool active)
    {
        for (int i = 0; i < CalibrationAssets.Length; i++)
        {
            CalibrationAssets[i].SetActive(active);
        }
    }

    public void TriggerStart(bool active)
    {
        startTest = active;
    }

    public void TriggerDebug(bool active)
    {
        printResult = active;
    }

    public void StartProprioTest()
    {
        this.PlayWaveform(wf_calibrationStart, true);

        TitleText.text = "Test Started!";
        InstructionText.text =  "Use the 'LEFT' and 'RIGHT' button to move the Fake hand" 
            + '\n' + " Bring the fake hand to where you think your right hand is." + '\n' + '\n' +
            "'Capture' the result when task is completed ";

        this.SetCalibrationAssets(true);

        startTest = false;

    }

    public void EndProprioTest()
    {
        this.PlayWaveform(wf_calibrationStart, true);

        TitleText.text = "Test Ended!";
        InstructionText.text = distance_Actual_Fake.ToString() + "-" + distance_Virtual_Fake.ToString();
        Debug.Log("Distance between Reference Hand and Controller hand: " + distance_Actual_Fake.ToString() + " centimeters");
        Debug.Log("Distance between Reference Hand and Controller hand: " + distance_Virtual_Fake.ToString() + " centimeters");

        this.SetCalibrationAssets(false);
        printResult = false;
    }

    void runDistanceCalculation()
    {
        if (FakeSGHand != null)
        {
            distance_Actual_Fake = (100f * (FakeSGHand.transform.position - ActualSGHand.transform.position));
            distance_Actual_Fake = this.Round(distance_Actual_Fake, 2);

            distance_Virtual_Fake = (100f * (FakeSGHand.transform.position - (ActualSGHand.transform.position - DriftUpdaterSource.positionOffset)));
            distance_Virtual_Fake = this.Round(distance_Virtual_Fake, 2);

            /*            distance = (float)System.Math.Round(distance.x, 0) + offsetInCM;*/
        }
    }

    /// Rounds Vector3
    public Vector3 Round(Vector3 vector3, int decimalPlaces = 2)
    {
        float multiplier = 1;
        for (int i = 0; i < decimalPlaces; i++)
        {
            multiplier *= 10f;
        }
        return new Vector3(
            Mathf.Round(vector3.x * multiplier) / multiplier,
            Mathf.Round(vector3.y * multiplier) / multiplier,
            Mathf.Round(vector3.z * multiplier) / multiplier);
    }

    void Update()
    {
        this.runDistanceCalculation();

        if (this.startTest)
        {
            this.StartProprioTest();        
        }

        if ((Input.GetKeyDown(this.debugKey)) || (this.printResult))
        {
            this.EndProprioTest();
        }

    }

}
