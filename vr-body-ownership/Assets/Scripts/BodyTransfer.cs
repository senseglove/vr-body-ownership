using System.Collections;
using System.Collections.Generic;
using SGCore;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR;
using static UnityEngine.ParticleSystem;

/// <summary> Sends a chosed waveform to one or more HapticGloves. </summary>
public class BodyTransfer : MonoBehaviour
{
	//---------------------------------------------------------------------------------------------------
	// Member Variables

	/// <summary> The Gloves to send the effect to  </summary>
	public SG.SG_HapticGlove[] hapticGloves = new SG.SG_HapticGlove[2];

    /// <summary> Scanner Particle System  </summary>
    public ParticleSystem[] scannerParticleSystem = new ParticleSystem[2];

    public GameObject[] preCalibrationAssets = new GameObject[0];

    [Header("Transfer Parameters")]
    [Range(0, 10)] public float transferEndTime = 5.0f;
    private bool startTimer = false;
    protected float timer = 0.0f;

    [Header("Waveforms")]
	public SG.SG_CustomWaveform Waveform1;
	public SG.SG_CustomWaveform Waveform2;
	public SG.SG_CustomWaveform Waveform3;

    private bool playingWF = false;
    private Coroutine wfTimeOut = null;

    [Header("Squeeze Parameters")]
    [Range(0, 10)] public float squeezeInStartTime = 1.0f;
    [Range(0, 10)] public float squeezeInEndTime = 2.0f;
    [Range(0, 10)] public float squeezeOutStartTime = 2.2f;
    [Range(0, 10)] public float squeezeOutEndTime = 3.5f; 

    private bool startSqueeze = false;
    protected int squeezeCount = 0;
    protected float squeeze = 0.0f;
    protected float minSqueeze = 0.0f;
    protected float maxSqueeze = 1.0f;

    [Header("Vibration Parameters")]
    [Header("Ramp Down -> Palm")]
    private bool startVibration = false;
    [Range(0, 10)] public float RampDownPalmStartTime = 1.0f;
    [Range(0, 10)] public float RampDownPalmStopTime = 1.5f;
    [Header("Ramp Up -> Palm")]
    [Range(0, 10)] public float RampUpPalmStartTime = 2.0f;
    [Range(0, 10)] public float RampUpPalmStopTime = 2.5f;
    [Header("Ramp Up -> Tip")]
    [Range(0, 10)] public float RampUpTipStartTime = 4.0f;
    [Range(0, 10)] public float RampUpTipStopTime = 6.0f;
    
    /// <summary> Hotkey to send the effect </summary>
    [Header("HotKeys & UI Elements")]
	public KeyCode StartBodyTransferKey = KeyCode.B;
    public KeyCode forceCompleteKey = KeyCode.Return;

    public UnityEngine.UI.Text HandPartText;


    //---------------------------------------------------------------------------------------------------
    // Functions

    public void SetCalibrationAssets(bool active)
    {
        for (int i = 0; i < preCalibrationAssets.Length; i++)
        {
            preCalibrationAssets[i].SetActive(active);
        }
    }

    private IEnumerator LockWavefromPlays(SG.SG_CustomWaveform toPlay)
    {
        playingWF = true;
        yield return new WaitForSeconds(toPlay.Duration);
        playingWF = false;
    }

    public void PlayWaveform(SG.SG_CustomWaveform toPlay, SG.VibrationLocation gloveLocation, bool overridePlaying)
    {
		foreach (SG.SG_HapticGlove glove in hapticGloves)
		{
			if (toPlay == null || glove == null)
			{
				return;
			}

			if (overridePlaying && this.wfTimeOut != null)
			{
				StopCoroutine(wfTimeOut);
				playingWF = false;
			}

			if (!playingWF) //Coroutine has stopped or been cancelled.
			{
				glove.SendCustomWaveform(toPlay, gloveLocation);
				this.wfTimeOut = StartCoroutine(LockWavefromPlays(toPlay));
			}
		}
    }

    /// <summary> Start Body-Transfer Sequence </summary>
    public void StartVRBodyTransfer()
    {
        HandPartText.text = "Started!";
        Debug.Log("Starting Body-Transfer Sequence");
        this.StartParticleSystem();
        this.startVibration = true;
        this.startSqueeze = true;
    }

    public void StopVRBodyTransfer()
    {
        this.startTimer = false;
        this.timer = 0.0f;
        Debug.Log("Resetting Timer, all start bools are set to false");


        Debug.Log("");
        HandPartText.text = "";
        SetCalibrationAssets(true);
    }

    /// <summary> Start Particle System </summary>
    public void StartParticleSystem()
    {
        foreach (ParticleSystem effect in scannerParticleSystem)
        {
            effect.Clear();
            effect.Play();
        }
    }

    /// <summary> Stop Particle System </summary>
    public void StopParticleSystem()
    {
        foreach (ParticleSystem effect in scannerParticleSystem)
        {
            effect.Stop();
            effect.Clear();
        }
    }
    /// <summary> Starts Vibrations based on timer </summary>
    public void StartVibrations()
    {
        /// Ramp In To Palm
        if (this.RampDownPalmStartTime < this.timer && this.timer < this.RampDownPalmStopTime)
        {
            this.PlayWaveform(Waveform1, SG.VibrationLocation.Palm_IndexSide, true);
            this.PlayWaveform(Waveform1, SG.VibrationLocation.Palm_PinkySide, true);
        }

        /// Ramp Out off Palm
        if (this.RampUpPalmStartTime < this.timer && this.timer < this.RampUpPalmStopTime)
        {
            this.PlayWaveform(Waveform2, SG.VibrationLocation.Palm_IndexSide, true);
            this.PlayWaveform(Waveform2, SG.VibrationLocation.Palm_PinkySide, true);
        }

        /// Ramp In To Fingers
        if (this.RampUpPalmStartTime < this.timer && this.timer < this.RampUpPalmStopTime)
        {
            this.PlayWaveform(Waveform3, SG.VibrationLocation.Thumb_Tip, true);
            this.PlayWaveform(Waveform3, SG.VibrationLocation.Index_Tip, true);
        }

        /// Stop Vibrations, and particle system
        if (this.timer > this.RampUpTipStopTime)
        {
            this.startVibration = false;
            this.StopParticleSystem();
        }

    }

    /// <summary> Starts Active Squeeze based on timer. </summary>
    public void StartSqueezing()
	{
        /// Ramp In: Squeeze In
        if (this.squeezeInStartTime < this.timer && this.timer < this.squeezeInEndTime)
        {
            if (this.squeeze < this.maxSqueeze)
            {
                this.squeeze += (1/(this.squeezeInEndTime - this.squeezeInStartTime)) * Time.deltaTime;
            }
            else
            {
                this.squeeze = this.maxSqueeze;
                this.squeezeCount++;

            }
        }
        /// Ramp out: Squeeze Out
        else if (this.squeezeOutStartTime < this.timer && this.timer < this.squeezeOutEndTime)
        {
            if (this.squeeze > this.minSqueeze)
            {
                this.squeeze -= (1 / (this.squeezeOutEndTime - this.squeezeOutStartTime)) * Time.deltaTime;
            }
            else
            {
                this.squeeze = this.minSqueeze;
                this.squeezeCount++;
            }
        }

        /// To Avoid further squeeze
        if (this.squeezeCount == 2)
        {
            this.startSqueeze = false; 
            this.squeezeCount = 0;
        }

        /// To Avoid going to negative
        if (this.squeeze < 0.0f) { this.squeeze = 0.0f; }

        /// Squeeeze the handd...muhahahhah
        foreach (SG.SG_HapticGlove glove in hapticGloves)
        {
            glove.QueueWristSqueeze(this.squeeze);
        }
    }

    /// <summary> Stops all Haptics </summary>
    public void StopHaptics()
    {
        foreach (SG.SG_HapticGlove glove in hapticGloves)
        {
            glove.StopAllVibrations();
            glove.StopWristSqueeze();
        }
    }

    //---------------------------------------------------------------------------------------------------
    // Monobehaviour
    void Start()
    {
    }

    void Update()
    {
        /// <summary> Timer starts for startVibrations() </summary>
        if (this.startTimer)
        {
            this.timer += Time.deltaTime;
           /* Debug.Log("Timer: " + this.timer);*/
        }
        else
        {
            /// Resetting Timer
            this.timer = 0.0f; //make sure we cannot go negative.
        }

        /// <summary> Transfer Call </summary>
        if (this.timer > this.transferEndTime) 
        {
            this.StopVRBodyTransfer();
        }


        /// <summary> Vibrations Call </summary>
        if (this.startVibration)
        {
            this.StartVibrations();
        }

        /// <summary> Active-Strap Squeeze Call </summary>
        if (this.startSqueeze)
        {
            this.StartSqueezing();
        }

        /// <summary> Start Squeeze, Vibrations, and Particle system </summary>
        if (Input.GetKeyDown(StartBodyTransferKey))
        {
            this.StartVRBodyTransfer();
            this.startTimer = true;
        }

        /// <summary> Force Stop Squeeze, Vibrations, and Particle system </summary>
        if (Input.GetKeyDown(forceCompleteKey))
        {
            this.startTimer = false;
            this.startSqueeze = false;
            this.startVibration = false;
            Debug.Log("ForceStop Body-Transfer Sequence");
            this.StopHaptics();
            this.StopParticleSystem();
            SetCalibrationAssets(true);
        }

    }
}



