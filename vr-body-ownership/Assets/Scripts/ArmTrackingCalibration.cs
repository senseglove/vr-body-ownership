using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary> For a SINGLE ARM? </summary>
public class ArmTrackingCalibration : MonoBehaviour
{
    /// <summary> The hand linked to the arm. </summary>
    public SG.SG_TrackedHand hand;

    public Transform hmdLocation;

    /// <summary> The 0-point for this 'hand's position- and rotation tracking </summary>
    public Transform handReferenceLocation;

    public Transform localReferenceFrame; //we'll use this to indicate which way is "Forward"

    /// <summary> Assets that should be visible/invisible during calibration. </summary>
    public GameObject[] preCalibrationAssets = new GameObject[0];
    public GameObject[] postCalibrationAssets = new GameObject[0]; 
    public GameObject[] SGAssets = new GameObject[0];

    /// <summary> How many times a calibration has been completed. </summary>
    public int CalibrationsCompleted { get; private set; }

    /// <summary> Whether or not a calibration sequence is running at the moment. </summary>
    public bool IsCalibrating { get; private set; }

    /// <summary> Plus and minus limits for hand rotation. </summary>
    public float angle_limit = 30f; // +/- degrees.
    public float position_limit = 0.05f; //+/- m
    /// <summary> The amount of time you're supposed to be stable on the location for. </summary>
    public float stableTime_s = 0.5f;

    public bool debugLines = true;

    protected float timer_handAtLocation = 0.0f;
    private bool wasInBounds = false;

    public SG.SG_CustomWaveform wf_calibrationStart;
    public SG.SG_CustomWaveform wf_correctLocation;
    public SG.SG_CustomWaveform wf_incorrectLocation;
    public SG.SG_CustomWaveform wf_calibrationComplete;

    private bool playingWF = false;
    private Coroutine wfTimeOut = null;

    /// <summary> Hotkeys </summary>
    [Header("HotKeys & UI Elements")]
    public KeyCode startCalibrationKey = KeyCode.C;
    public KeyCode debugKey = KeyCode.L;
    public KeyCode forceCompleteKey = KeyCode.Return;

    public UnityEngine.UI.Text TitleText;
    public UnityEngine.UI.Text InstructionText;


    /// <summary> Move left or right depending on the personal hand. </summary>
    private Vector3 hmd_to_hand_R = new Vector3(0.28f, -0.60f, 0.15f);
    private Quaternion hmd_to_handRotation_R = Quaternion.Euler( -90, 0, -120 );
    private Quaternion hmd_to_handRotation_L = Quaternion.Euler(  90, 0, 60 );



    public void SetCalibrationAssets(bool active)
    {
        for (int i=0; i<preCalibrationAssets.Length; i++)
        {
            preCalibrationAssets[i].SetActive(active);
        }
    }



    private IEnumerator LockWavefromPlays(SG.SG_CustomWaveform toPlay)
    {
        playingWF = true;
        yield return new WaitForSeconds(toPlay.Duration);
        playingWF = false;
    }

    public void PlayWaveform(SG.SG_CustomWaveform toPlay, bool overridePlaying)
    {
        if (toPlay == null || this.hand == null)
        {
            return;
        }

        if (overridePlaying && this.wfTimeOut != null)
        {
            StopCoroutine(wfTimeOut);
            playingWF = false;
        }
        if ( !playingWF ) //Coroutine has stopped or been cancelled.
        {
            hand.SendCustomWaveform(toPlay, SG.VibrationLocation.Palm_PinkySide);
            hand.SendCustomWaveform(toPlay, SG.VibrationLocation.Palm_IndexSide);
            hand.SendCustomWaveform(toPlay, SG.VibrationLocation.Thumb_Tip);
            hand.SendCustomWaveform(toPlay, SG.VibrationLocation.Index_Tip);
            this.wfTimeOut = StartCoroutine(LockWavefromPlays(toPlay));
        }
    }

    public void GetCalibratedLocation(Vector3 wristPosition, Quaternion wristRotation, out Vector3 calibratedPosition, out Quaternion calibratedRotation)
    {
        if (this.IsCalibrating || CalibrationsCompleted < 1) //we are currently calibrating of have not yet completed a proper calibration
        {
            calibratedPosition = Vector3.zero;
            calibratedRotation = Quaternion.identity;
        }
        else
        {
            Vector3 relativePos = wristPosition - this.handReferenceLocation.position; //this is in the world frame.
            calibratedPosition = this.localReferenceFrame.InverseTransformDirection(relativePos);

            calibratedRotation = Quaternion.identity;
        }
    }

    public void StartArmCalibration(bool forceRestart)
    {
        if (this.IsCalibrating)
        {
            if (forceRestart)
            {
                this.CancelCalibration();
            }
            else
            {
                Debug.Log("A calibration sequence is already started for the " + (this.hand.TracksRightHand() ? "Right" : "Left") + " arm");
            }
        }

        if (!this.IsCalibrating)
        {
            TitleText.text = "Sequence started:";
            InstructionText.text = "Place your Right Hand on the Table and match the visual vue provided.";

            //Place handReferenceLocation at the correct location
            Debug.Log("Started Calibration for the " + (this.hand.TracksRightHand() ? "Right" : "Left") + " arm.");
            this.IsCalibrating = true;
            SetCalibrationAssets(true);
            this.timer_handAtLocation = 0.0f;
            this.PlayWaveform(wf_calibrationStart, true);

            wasInBounds = this.HandWithinBounds();
        }
    }

    public bool HandWithinBounds()
    {
        SG.SG_HandPose latestPose = this.hand.GetHandPose(SG.SG_TrackedHand.TrackingLevel.RealHandPose);

        //test 1: Position
        float refDist = (latestPose.wristPosition - this.handReferenceLocation.position).magnitude;
        if (refDist > this.position_limit) //too far!
        {
            if (debugLines)
            {
                Debug.DrawLine(this.handReferenceLocation.position, latestPose.wristPosition, Color.blue); //you're too far!
            }
            return false;
        }

        //test 2: Align fingers
        Vector3 refFwd = this.handReferenceLocation.rotation * new Vector3(1, 0, 0);
        Vector3 handFwd = latestPose.wristRotation * new Vector3(1, 0, 0);
        if ( Vector3.Angle(refFwd, handFwd) > this.angle_limit )
        {
            if (debugLines)
            {
                Debug.DrawLine(this.handReferenceLocation.position, latestPose.wristPosition, Color.red); //you're too far!
            }
            return false;
        }

        //test 3: Align hand palms
        Vector3 refUp = this.handReferenceLocation.rotation * new Vector3(0, 1, 0);
        Vector3 handUp = latestPose.wristRotation * new Vector3(0, 1, 0);
        if (Vector3.Angle(refUp, handUp) > this.angle_limit)
        {
            if (debugLines)
            {
                Debug.DrawLine(this.handReferenceLocation.position, latestPose.wristPosition, Color.yellow); //you're too far!
            }
            return false;
        }

        if (debugLines)
        {
            Debug.DrawLine(this.handReferenceLocation.position, latestPose.wristPosition, Color.green); //you're too far!
        }
        return true;
    }

    /// <summary> End calibration, but doesn't mark it as succesful. </summary>
    public void CancelCalibration()
    {
        if (this.IsCalibrating)
        {
            this.IsCalibrating = false;
            SetCalibrationAssets(false);

            this.PlayWaveform(wf_calibrationComplete, true);
        }
    }

    public void CompleteArmCalibration()
    {
        if (this.IsCalibrating)
        {
            this.IsCalibrating = false;

            Debug.Log("Completed Calibration for the " + (this.hand.TracksRightHand() ? "Right" : "Left") + " arm.");
            this.CalibrationsCompleted++; //we've calibrated one calibratin
            //SetCalibrationAssets(this.CalibrationsCompleted > 0);
            SetCalibrationAssets(false);
            //Place the reference location at the calibrated end position.
            SG.SG_HandPose latestPose = this.hand.GetHandPose(SG.SG_TrackedHand.TrackingLevel.RealHandPose);

            

            this.handReferenceLocation.rotation = latestPose.wristRotation;
            this.handReferenceLocation.position = latestPose.wristPosition;

            this.PlayWaveform(wf_calibrationComplete, true);

            for (int i = 0; i < postCalibrationAssets.Length; i++)
            {
                postCalibrationAssets[i].SetActive(true);
            }

            for (int i = 0; i < SGAssets.Length; i++)
            {
                SGAssets[i].SetActive(false);
            }
            TitleText.text = "";

            InstructionText.text =  "Maintain the position of your right hand on the table." + '\n' + '\n' +
                "You will now continue this experiment using only your LEFT HAND." + '\n' + '\n' +
                "Press the 'Start Test' sphere to continue";

        }
    }


    // Start is called before the first frame update
    void Start()
    {
        SetCalibrationAssets(this.CalibrationsCompleted > 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.IsCalibrating) //Currently Calibrating
        {
            bool inBounds = HandWithinBounds();
            if (inBounds)
            {
                this.timer_handAtLocation += Time.deltaTime;
            }
            else
            {
                this.timer_handAtLocation -= Time.deltaTime;
                if (this.timer_handAtLocation < 0.0f) { timer_handAtLocation = 0.0f; } //make sure we cannae go negative.
            }

            if (this.timer_handAtLocation >= this.stableTime_s)
            {
                this.CompleteArmCalibration();
            }
            else if (Input.GetKeyDown(this.forceCompleteKey))
            {
                this.CompleteArmCalibration();
            }

            if (this.IsCalibrating) //we didn't finish playing
            {
                if (inBounds && !wasInBounds)
                {
                    this.PlayWaveform(wf_correctLocation, true);
                }
                else if (!inBounds && wasInBounds)
                {
                    this.PlayWaveform(wf_correctLocation, true);
                }
            }
            wasInBounds = inBounds;
        }
        if (Input.GetKeyDown(this.startCalibrationKey))
        {
            this.StartArmCalibration(false);
        }    

    }
}
