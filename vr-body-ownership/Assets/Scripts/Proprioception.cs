using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
	/// <summary> Sends a chosed waveform to one or more HapticGloves. </summary>
	public class Proprioception : MonoBehaviour
	{
		//---------------------------------------------------------------------------------------------------
		// Member Variables

		/// <summary> The Gloves to send the effect to  </summary>
		public SG_HapticGlove[] hapticGloves = new SG_HapticGlove[2];
		public SG_Waveform Waveform1;
		public SG_Waveform Waveform2;

		public GameObject LeftHandSource;
		public GameObject RightHandSource;

		public bool LeftObjectSnapped = false;
		public bool RightObjectSnapped = false;

		public int countDownTime = 7;

		private float CallibPosition;
		private float ProprioceptiveDrift;

		/// <summary> Hotkeys </summary>
		[Header("HotKeys & UI Elements")]
		public KeyCode CaptureKey = KeyCode.Space;
		public UnityEngine.UI.Text LeftHandPartText;
		public UnityEngine.UI.Text RightHandPartText;
		public UnityEngine.UI.Text ProprioceptiveDriftText;

		//---------------------------------------------------------------------------------------------------
		// Functions
		public void FlipLeftSnapBool()
		{
			if (LeftObjectSnapped == false)
			{
				LeftObjectSnapped = true;
				LeftHandPartText.text = "Grabbed!";
				//hapticGloves[0].SendCmd(Waveform1);
				ProprioceptiveDriftText.text = "";
			}
			else {LeftHandPartText.text = "Not Grabbed!";}
		}

		public void FlipRightSnapBool()
		{
			if (RightObjectSnapped == false)
			{
				RightObjectSnapped = true;
				RightHandPartText.text = "Grabbed!";
				//hapticGloves[1].SendCmd(Waveform1);
				ProprioceptiveDriftText.text = "";	
			}
			else {RightHandPartText.text = "Not Grabbed!";}
		}

		public void CalculateProprioceptiveDrift()
		{
			// if (LeftObjectSnapped == false && RightObjectSnapped == false)
			// {
			// 	CallibPosition =  Vector3.Distance(LeftHandSource.transform.position,RightHandSource.transform.position);
			// 	ProprioceptiveDriftText.text = "Captured!";
			// 	Debug.Log("Callibration Position = " + CallibPosition.ToString("F3"));
			// 	hapticGloves[0].SendCmd(Waveform2);
			// 	hapticGloves[1].SendCmd(Waveform2);
			// }

			if (LeftObjectSnapped && RightObjectSnapped)
			{
				ProprioceptiveDrift =  Vector3.Distance(LeftHandSource.transform.position,RightHandSource.transform.position);
				// ProprioceptiveDrift = ProprioceptiveDrift - CallibPosition;
				ProprioceptiveDriftText.text = "Captured!";
				Debug.Log("Proprioceptive Drift = " + ProprioceptiveDrift.ToString("F3"));
				//hapticGloves[0].SendCmd(Waveform2);
				//hapticGloves[1].SendCmd(Waveform2);
			}
		}

		IEnumerator CountDownToStart()
		{
			while(countDownTime > 0)
			{
				ProprioceptiveDriftText.text = countDownTime.ToString();
				yield return new WaitForSeconds(1f);
				countDownTime--;
			}

		}

		public void StartProprioceptiveDrift()
		{
			this.countDownTime = 7;
			StartCoroutine(CountDownToStart());
			Invoke("CalculateProprioceptiveDrift",this.countDownTime);
		}

		//---------------------------------------------------------------------------------------------------
		// Monobehaviour

		// Use this for initialization
		private void Start()
		{	
			
		}

		// Update is called once per frame
		void Update()
		{
			if (Input.GetKeyDown(CaptureKey))
			{	
				this.StartProprioceptiveDrift();
			}

		}
	}
}


