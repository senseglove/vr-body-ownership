using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DriftUpdater : MonoBehaviour
{

    public Transform reference;
    public Vector3 positionOffset;

    void Update()
    {
        this.transform.SetPositionAndRotation(reference.position + positionOffset, reference.rotation);
    }
}
