using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour
{
    public GameObject player;

    [SerializeField] public float movementSpeed;
    private bool isMinuxX = false;
    private bool isPlusX = false;
    private bool isMinuxY = false;
    private bool isPlusY = false;
    private bool isMinuxZ = false;
    private bool isPlusZ = false;

    void Update()
    {
        if (isMinuxX)
        {
            player.transform.position += new Vector3(-movementSpeed, 0, 0);
        }

        if (isPlusX)
        {
            player.transform.position += new Vector3(+movementSpeed, 0, 0);
        }

        if (isMinuxY)
        {
            player.transform.position += new Vector3(0,-movementSpeed, 0);
        }

        if (isPlusY)
        {
            player.transform.position += new Vector3(0, +movementSpeed, 0);
        }

        if (isMinuxZ)
        {
            player.transform.position += new Vector3(0, 0, -movementSpeed);
        }

        if (isPlusZ)
        {
            player.transform.position += new Vector3(0, 0, +movementSpeed);
        }

    }

    public void isTriggerMinuxX(bool value)
    {
        isMinuxX = value;
    }

    public void isTriggerPlusX(bool value)
    {
        isPlusX = value;
    }

    public void isTriggerMinuxY(bool value)
    {
        isMinuxY = value;
    }

    public void isTriggerPlusY(bool value)
    {
        isPlusY = value;
    }

    public void isTriggerMinuxZ(bool value)
    {
        isMinuxZ = value;
    }

    public void isTriggerPlusZ(bool value)
    {
        isPlusZ = value;
    }
}
