using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

/// <summary> Change Scene on Button Click. </summary>
public class SceneChanger : MonoBehaviour
{
    public KeyCode ChangeSceneKey = KeyCode.RightArrow;
    public KeyCode ResetSceneKey = KeyCode.LeftArrow;
    public UnityEvent ChangeSceneEvent = new UnityEvent();
    public UnityEvent ResetSceneEvent = new UnityEvent();
    public int NextSceneIndex;

    [ContextMenu("Change Scene")]
    public void ChangeScene()
    {
        SceneManager.LoadScene(NextSceneIndex);
    }

    [ContextMenu("Reset Scene")]
    public void ResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void Update()
    {
        if (Input.GetKeyDown(ChangeSceneKey))
        {
            this.ChangeSceneEvent.Invoke();
        }

        if (Input.GetKeyDown(ResetSceneKey))
        {
            this.ResetSceneEvent.Invoke();
        }
    }

}
